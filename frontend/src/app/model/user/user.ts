export class User {
  id: bigint;
  name: string;
  email: string;
  phoneNumber: string;
}
