export class Book {
  id: bigint;
  title: string;
  author: string;
  genre: string;
  year: bigint;
}
