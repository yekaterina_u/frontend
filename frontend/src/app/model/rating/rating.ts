export class Rating {
  id: bigint;
  value: bigint;
  comment: string;
}
