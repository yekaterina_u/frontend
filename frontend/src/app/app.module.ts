ng import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { User } from './user.service/user.service.component';
import { UserListComponent } from './user-list/user-list.component';
import { BookListComponent } from './book-list/book-list.component';
import { MessageListComponent } from './message-list/message-list.component';
import { RatingListComponent } from './rating-list/rating-list.component';

@NgModule({
  declarations: [
    AppComponent,
    User.ServiceComponent,
    UserListComponent,
    BookListComponent,
    MessageListComponent,
    RatingListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
